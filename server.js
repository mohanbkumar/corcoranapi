var dotenv = require('dotenv');

dotenv.config({
    path: './src/environment/.env'
});

dotenv.load();

var express     = require('express'),
    boom        = require('express-boom'),
    cors        = require('cors'),
    bodyParser  = require('body-parser'),
    logger      = require('morgan')
    routes      = require('./src/routes/routes.js');


var app = express();

// TODO: Use this for logging for API monitoring for now log to console
//app.use(logger('combined'));
app.use(logger(':remote-addr - [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));
//app.use(logger('dev'));

app.use(boom());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

// app.use(function (req, res, next) {
//     res.setHeader('Access-Control-Allow-Origin', '*');
//     res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE');
//     res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
//     next();
// });

routes(app, express, '/api');

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500).json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500).json({
        message: err.message,
        error: {}
    });
});

var server = app.listen(
    process.env.API_PORT || 80,
    process.env.API_SERVER || 'ec2-13-232-81-173.ap-south-1.compute.amazonaws.com', function () {
        console.log('Corcorn API running on port: ' + process.env.API_PORT);
    });

module.exports = {
    server: server,
    app: app
};
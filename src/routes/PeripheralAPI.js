module.exports = function (models, router) {

    return router
        .Router()
        .get('', function (req, res) {
            models
                .Peripheral
                .findAll({
                    limit :30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

};
var models = require('../models/index.js');
module.exports = function (app, router, routeStart) {
    app.use(routeStart + '/agent', require('./AgentAPI')(models, router));
    app.use(routeStart + '/Announcement', require('./AnnouncementAPI')(models, router));    
    app.use(routeStart + '/BrokerageAgreement', require('./BrokerageAgreementAPI')(models, router));    
    app.use(routeStart + '/Brokerage', require('./BrokerageAPI')(models, router));    
    app.use(routeStart + '/broker', require('./BrokerAPI')(models, router));    
    app.use(routeStart + '/Calendar', require('./CalendarAPI')(models, router));    
    app.use(routeStart + '/Directory', require('./DirectoryAPI')(models, router));    
    app.use(routeStart + '/Event', require('./EventAPI')(models, router));    
    app.use(routeStart + '/Inquiry', require('./InquiryAPI')(models, router));    
    app.use(routeStart + '/Master', require('./MasterAPI')(models, router));
    app.use(routeStart + '/Offer', require('./OfferAPI')(models, router));
    app.use(routeStart + '/Peripheral', require('./PeripheralAPI')(models, router));
    app.use(routeStart + '/Prospect', require('./ProspectAPI')(models, router));
    app.use(routeStart + '/site', require('./SiteAPI')(models, router));
    app.use(routeStart + '/Users', require('./UserAPI')(models, router));
    app.use(routeStart + '/Visit', require('./VisitAPI')(models, router));

    
    
    app.use('/', function (req, res) {
        res
            .boom
            .notImplemented('http://' + req.headers.host + req.url + ' is not implemented');
    });
}
module.exports = function (models, router) {

    return router
        .Router()
        .get('', function (req, res) {
            models
                .Directory
                .findAll({
                    limit :300,
                    // order: [[models.Directory, 'FirstName', 'asc']]
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

};
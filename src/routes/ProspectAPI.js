module.exports = function (models, router) {
    var atob = require('atob');

    return router
        .Router()
        .get('', function (req, res) {
            var params = {};
            params = JSON.parse(atob(req.query.search));
            var query = {};
            if (params) {
                if (params['StartDate'] != undefined && params['EndDate'] != undefined) {
                    query['DateCreated'] = { $between: [params['StartDate'].slice(0, 10), params['EndDate'].slice(0, 10)] }
                } else if (params['StartDate'] != undefined || params['EndDate'] != undefined) {
                    query['DateCreated'] = params['StartDate'] != undefined ? params['StartDate'].slice(0, 10) : params['EndDate'].slice(0, 10);
                }
                for (var key in params) {
                    if (params[key] != '' && key != 'StartDate' && key != 'EndDate') {
                        query[key] = { $like: params[key] + '%' };
                    }
                }
            }
            query = Object.getOwnPropertyNames(query).length > 0 ? { $and: query } : {};
            models
                .Prospect
                .findAll({
                    where: query,
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        .post('', function (req, res) {
            let prospect = req.body.prospect;
            //delete prospect.key;
            if (prospect.key) {
                models
                    .Prospect
                    .update(prospect, {
                        where: {
                            'ProspectID': prospect.key
                        }
                    }).then(function (prospect) {
                        res
                            .json(prospect);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    });
            } else {
                models
                    .Prospect
                    .create(prospect)
                    .then(function (prospect) {
                        res
                            .json(prospect);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    })
            }
        })

        .delete('', function (req, res) {
            let prospect = req.query.prospect;
            //delete prospect.key;
            models
                .Prospect
                .destroy({
                    where: {
                        'prospectid': prospect
                    }
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .badData(error);
                })
        })
};
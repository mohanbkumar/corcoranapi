module.exports = function (models, router) {
    var atob = require('atob');
    return router
        .Router()
        .get('', function (req, res) {
            var params = {};
            if (req.query.search !== "" && req.query.search !== undefined) {
                params = JSON.parse(atob(req.query.search));
                var query = {};
                if (params) {
                    for (var key in params) {
                        if (params[key] != '') {
                            query[key] = { $like: params[key] + '%' };
                        }
                    }
                }
                query = Object.getOwnPropertyNames(query).length > 0 ? { $and: query } : {};
            }

            models
                .Site
                .findAll({
                    where: query,
                    include: [{
                        model: models.SiteDev,
                        as: 'SiteSiteDev',
                        required: false,

                    }, {
                        model: models.SiteLegal,
                        as: 'SiteLegal',
                        required: false
                    }],
                    raw: true,
                    group: ["Site.ID"],
                    order: [
                        ['ID', 'DESC']
                    ]
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        //Save Site Data
        .post('', function (req, res) {
            let site = req.body.site;
            delete site.SiteCode;
            const sitedev = site.SiteDev;
            const sitelegal = site.Legal;
            if (site.key) {
                models
                    .Site
                    .update(site, {
                        where: {
                            'key': site.key
                        }
                    }).then(function (result) {
                        models
                            .SiteDev
                            .update(sitedev, {
                                where: {
                                    'SiteID': site.key
                                }
                            })
                            .then(function (sitedev) {
                                sitelegal.SiteProfileID = sitedev.Id
                                models
                                    .SiteLegal
                                    .update(sitelegal, {
                                        where: {
                                            'SiteID': site.key
                                        }
                                    })
                                    .then(function (sitelegal) {
                                        res
                                            .json(site);
                                    })
                                    .catch(function () {
                                        res
                                            .boom
                                            .badData(error);
                                    })

                            })
                            .catch(function (error) {
                                res
                                    .boom
                                    .badData(error);
                            })


                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    });
            } else {
                models
                    .Site
                    .create(site)
                    .then(function (result) {
                        if (result.key) {
                            sitedev.SiteID = result.key;
                            sitelegal.SiteID = result.key;
                            // Saving Site Ownership & Desgin
                            models
                                .SiteDev
                                .create(sitedev)
                                .then(function (sitedev) {
                                    sitelegal.SiteProfileID = sitedev.Id
                                    models
                                        .SiteLegal
                                        .create(sitelegal)
                                        .then(function (sitelegal) {
                                            res.json(result);
                                        })
                                        .catch(function (error) {
                                            res
                                                .boom
                                                .badData(error);
                                        })
                                })
                                .catch(function (error) {
                                    res
                                        .boom
                                        .badData(error);
                                })

                        } else {
                            res.json(result);
                        }
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    })
            }

        })

        .delete("", function (req, res) {
            if (req.query.site) {
                models
                    .Site
                    .destroy({
                        where: {
                            ID: {
                                $in: [req.query.site]
                            }
                        }
                    })
                    .then(function (site) {
                        models
                            .SiteDev
                            .destroy({
                                where: {
                                    SiteID: {
                                        $in: [req.query.site]
                                    }
                                }
                            })
                            .then(function (sitedev) {
                                models
                                    .SiteLegal
                                    .destroy({
                                        where: {
                                            SiteID: {
                                                $in: [req.query.site]
                                            }
                                        }
                                    })
                                    .then(function (site) {
                                        res.json(site);
                                    })
                                    .catch(function (error) {
                                        res.boom.badData(error);
                                    })
                            })

                    })
                    .catch(function (error) {
                        res.boom.badData(error);
                    });
            }
        })

};
module.exports = function (models, router) {

    return router
        .Router()
        // Function to fetch States
        .get('/states', function (req, res) {
            models
                .State
                .findAll()
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        .get('/country', function (req, res) {
            models
                .Country
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/calltype', function (req, res) {
            models
                .CallType
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/unit', function (req, res) {
            models
                .Unit
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/origin', function (req, res) {
            models
                .Origin
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/residence', function (req, res) {
            models
                .Residence
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/PurchaseReason', function (req, res) {
            models
                .PurchaseReason
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })        
        .get('/PriceRange', function (req, res) {
            models
                .PriceRange
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/PreferredLayout', function (req, res) {
            models
                .PreferredLayout
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })        
        .get('/prospectType', function (req, res) {
            models
                .ProspectType
                .findAll()
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
        .get('/referralsource', function (req, res) {
            models
                .ReferralSource
                .findAll({
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        // Function to fetch Status
        .get('/status', function (req, res) {
            models
                .Status
                .findAll()
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        }) 
        // // Function all agents (SP)
        .get('/agents', function (req, res) {            
            models.sequelize.query('CALL sp_getAllAgents();').then(function(response){                    
                res
                .json(response);
               }).error(function(err){
                 res.boom
                .notAcceptable(error);
            });               
        })

        // Function to fetch Status
        .get('/companies', function (req, res) {
            models
                .Company
                .findAll()
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })
};
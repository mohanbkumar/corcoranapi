module.exports = function (models, router) {
    var atob = require('atob');

    return router
        .Router()
        .get('', function (req, res) {
            var params = {};
            params = JSON.parse(atob(req.query.search));
            var query = {};
            if (params) {
                if (params['StartDate'] != undefined && params['EndDate'] != undefined) {
                    query['DateCreated'] = { $between: [params['StartDate'], params['EndDate']] }
                } else if (params['StartDate'] != undefined || params['EndDate'] != undefined) {
                    query['DateCreated'] = params['StartDate'] != undefined ? params['StartDate'] : params['EndDate'];
                }
                for (var key in params) {
                    if (params[key] != '' && key != 'StartDate' && key != 'EndDate') {
                        query[key] = { $like: params[key] + '%' };
                    }
                }
            }
            query = Object.getOwnPropertyNames(query).length > 0 ? { $and: query } : {};
            models
                .Inquiry
                .findAll({
                    where: query,
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        .post('', function (req, res) {
            let inquiry = req.body.inquiry;
            //delete inquiry.key;
            if (inquiry.key) {
                models
                    .Inquiry
                    .update(inquiry, {
                        where: {
                            'InquiryID': inquiry.key
                        }
                    }).then(function (inquiry) {
                        res
                            .json(inquiry);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    });
            } else {
                models
                    .Inquiry
                    .create(inquiry)
                    .then(function (inquiry) {
                        res
                            .json(inquiry);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    })
            }
        })

        .delete('', function (req, res) {
            let inquiry = req.query.inquiry;
            //delete inquiry.key;
            models
                .Inquiry
                .destroy({
                    where: {
                        'InquiryID': inquiry
                    }
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .badData(error);
                })
        })

};
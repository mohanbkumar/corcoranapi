module.exports = function (models, router) {
    var atob = require('atob');
    return router
        .Router()
        .get('', function (req, res) {
            var params = JSON.parse(atob(req.query.search));
            var query = {};
            if (params) {
                if (params['StartDate'] != undefined && params['EndDate'] != undefined) {
                    query['DateCreated'] = { $between: [params['StartDate'].slice(0,10), params['EndDate'].slice(0,10)] }
                } else if (params['StartDate'] != undefined || params['EndDate'] != undefined) {
                    query['DateCreated'] = params['StartDate'] != undefined ? params['StartDate'].slice(0,10) : params['EndDate'].slice(0,10);
                }
                for (var key in params) {
                    if (params[key] != '' && key != 'StartDate' && key != 'EndDate') {
                        query[key] = { $like: params[key] + '%' };
                    }
                }
            }
            query = Object.getOwnPropertyNames(query).length > 0 ? { $and: query } : {};
            models
                .Broker
                .findAll({
                    where: query,
                    limit: 30
                })
                .then(function (data) {
                    res
                        .json(data);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                });
        })

        .post('', function (req, res) {
            let broker = req.body.broker;

            if (broker.key) {
                models
                    .Broker
                    .update(broker, {
                        where: {
                            'BrokerID': broker.key
                        }
                    }).then(function (broker) {
                        res
                            .json(broker);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    });
            } else {
                models
                    .Broker
                    .create(broker)
                    .then(function (broker) {
                        res
                            .json(broker);
                    })
                    .catch(function (error) {
                        res
                            .boom
                            .badData(error);
                    })
            }

        })


};
module.exports = function (models, router) {
    return router
        .Router()
        .get('', function (req, res) {
            models
                .Users
                .findAll({
                    include: [{
                        model       : models.Role,
                        as          : 'UsersRole',
                        required    : true
                    }],
                    attributes: [
                        "UserID",
                        [models.sequelize.literal("CONCAT(FirstName, ' , ', LastName)"), "FullName"]
                    ],
                    raw: true
                })
                .then(function (users) {
                    res
                        .json(users);
                })
                .catch(function (error) {
                    res
                        .boom
                        .notAcceptable(error);
                })
        })
}
CREATE DATABASE  IF NOT EXISTS `csonsite2` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `csonsite2`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: csonsite2
-- ------------------------------------------------------
-- Server version	5.6.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `UserID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleID` int(11) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(12) NOT NULL,
  `FirstName` varchar(20) DEFAULT NULL,
  `LastName` varchar(20) DEFAULT NULL,
  `Title` varchar(100) DEFAULT NULL,
  `Phone` varchar(12) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Status` int(11) NOT NULL,
  `DateCreated` datetime(3) NOT NULL,
  `CreatedByID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Cole','123','Cole','Tricia','SA',NULL,NULL,1,'2018-05-26 00:00:00.000',1),(3,2,'Cordeiro','123','Cordeiro','Dan','EM',NULL,NULL,1,'2018-05-26 00:00:00.000',2),(4,3,'Fisher','123','Fisher','Beth','OM',NULL,NULL,1,'2018-05-26 00:00:00.000',3),(5,4,'Flynn','123','Flynn','Megan','MD',NULL,NULL,1,'2018-05-26 00:00:00.000',4),(6,5,'Gromet','123','Gromet','David','PM',NULL,NULL,1,'2018-05-26 00:00:00.000',5),(7,6,'Kellermeyer','123','Kellermeyer','Gabe','SD',NULL,NULL,1,'2018-05-26 00:00:00.000',6),(8,7,'Lansill','123','Lansill','James','SA',NULL,NULL,1,'2018-05-26 00:00:00.000',7),(9,8,'Novotny','123','Novotny','Sharon','SG',NULL,NULL,1,'2018-05-26 00:00:00.000',8),(10,9,'OKeefe','123','OKeefe','Shelley','FI',NULL,NULL,1,'2018-05-26 00:00:00.000',9),(11,10,'Peerless','123','Andrew','Beth','DE',NULL,NULL,1,'2018-05-26 00:00:00.000',10);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-26  0:49:46

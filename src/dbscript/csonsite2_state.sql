/*
-- Query: SELECT * FROM csonsite2.state
LIMIT 0, 1000

-- Date: 2018-05-21 12:46
*/
INSERT INTO `state` (`Id`,`State`) VALUES (1,'AK');
INSERT INTO `state` (`Id`,`State`) VALUES (2,'AL');
INSERT INTO `state` (`Id`,`State`) VALUES (3,'AR');
INSERT INTO `state` (`Id`,`State`) VALUES (4,'AZ');
INSERT INTO `state` (`Id`,`State`) VALUES (5,'CA');
INSERT INTO `state` (`Id`,`State`) VALUES (6,'CO');
INSERT INTO `state` (`Id`,`State`) VALUES (7,'CT');
INSERT INTO `state` (`Id`,`State`) VALUES (8,'DC');
INSERT INTO `state` (`Id`,`State`) VALUES (9,'DE');
INSERT INTO `state` (`Id`,`State`) VALUES (10,'FL');
INSERT INTO `state` (`Id`,`State`) VALUES (11,'GA');
INSERT INTO `state` (`Id`,`State`) VALUES (12,'HI');
INSERT INTO `state` (`Id`,`State`) VALUES (13,'IA');
INSERT INTO `state` (`Id`,`State`) VALUES (14,'ID');
INSERT INTO `state` (`Id`,`State`) VALUES (15,'IL');
INSERT INTO `state` (`Id`,`State`) VALUES (16,'IN');
INSERT INTO `state` (`Id`,`State`) VALUES (17,'KS');
INSERT INTO `state` (`Id`,`State`) VALUES (18,'KY');
INSERT INTO `state` (`Id`,`State`) VALUES (19,'LA');
INSERT INTO `state` (`Id`,`State`) VALUES (20,'MA');
INSERT INTO `state` (`Id`,`State`) VALUES (21,'MD');
INSERT INTO `state` (`Id`,`State`) VALUES (22,'ME');
INSERT INTO `state` (`Id`,`State`) VALUES (23,'MI');
INSERT INTO `state` (`Id`,`State`) VALUES (24,'MN');
INSERT INTO `state` (`Id`,`State`) VALUES (25,'MO');
INSERT INTO `state` (`Id`,`State`) VALUES (26,'MS');
INSERT INTO `state` (`Id`,`State`) VALUES (27,'MT');
INSERT INTO `state` (`Id`,`State`) VALUES (28,'NC');
INSERT INTO `state` (`Id`,`State`) VALUES (29,'ND');
INSERT INTO `state` (`Id`,`State`) VALUES (30,'NE');
INSERT INTO `state` (`Id`,`State`) VALUES (31,'NH');
INSERT INTO `state` (`Id`,`State`) VALUES (32,'NJ');
INSERT INTO `state` (`Id`,`State`) VALUES (33,'NM');
INSERT INTO `state` (`Id`,`State`) VALUES (34,'NV');
INSERT INTO `state` (`Id`,`State`) VALUES (35,'NY');
INSERT INTO `state` (`Id`,`State`) VALUES (36,'OH');
INSERT INTO `state` (`Id`,`State`) VALUES (37,'OK');
INSERT INTO `state` (`Id`,`State`) VALUES (38,'OR');
INSERT INTO `state` (`Id`,`State`) VALUES (39,'PA');
INSERT INTO `state` (`Id`,`State`) VALUES (40,'RI');
INSERT INTO `state` (`Id`,`State`) VALUES (41,'SC');
INSERT INTO `state` (`Id`,`State`) VALUES (42,'SD');
INSERT INTO `state` (`Id`,`State`) VALUES (43,'TN');
INSERT INTO `state` (`Id`,`State`) VALUES (44,'TX');
INSERT INTO `state` (`Id`,`State`) VALUES (45,'UT');
INSERT INTO `state` (`Id`,`State`) VALUES (46,'VA');
INSERT INTO `state` (`Id`,`State`) VALUES (47,'VT');
INSERT INTO `state` (`Id`,`State`) VALUES (48,'WA');
INSERT INTO `state` (`Id`,`State`) VALUES (49,'WI');
INSERT INTO `state` (`Id`,`State`) VALUES (50,'WV');
INSERT INTO `state` (`Id`,`State`) VALUES (51,'WY');

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('RequestLogin', {
    RequestLoginID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Username: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    Password: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    Code: {
      type: DataTypes.INTEGER(6),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'RequestLogin'
  });
};

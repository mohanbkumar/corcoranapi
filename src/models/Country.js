/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Country', {
    CountryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Country: {
      type: DataTypes.STRING(48),
      allowNull: true
    }
  }, {
      tableName: 'Country'
    });
};

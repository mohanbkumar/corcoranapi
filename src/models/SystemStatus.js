/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SystemStatus', {
    StatusID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SystemEnabled: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    }
  }, {
    tableName: 'SystemStatus'
  });
};

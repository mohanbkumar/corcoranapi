/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('BrokerBrokerage', {
    BrokerBrokerageID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    BrokerID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    BrokerageID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CreatedDate: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'BrokerBrokerage'
  });
};

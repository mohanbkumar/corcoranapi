/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Role', {
    key: {
      field: 'RoleID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    RoleName: {
      type: DataTypes.STRING(50),
      allowNull: false
    }
  }, {
      tableName: 'Role'
    });
};

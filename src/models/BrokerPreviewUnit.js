/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('BrokerPreviewUnit', {
    BrokerPreviewUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    BrokerPreviewID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'BrokerPreviewUnit'
  });
};

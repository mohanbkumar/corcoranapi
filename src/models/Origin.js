/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Origin', {
    OriginID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    OriginName: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'Origin'
  });
};

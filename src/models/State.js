/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('State', {
    Id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    State: {
      type: DataTypes.STRING(3),
      allowNull: false
    }
  }, {
      tableName: 'State'
    });
};

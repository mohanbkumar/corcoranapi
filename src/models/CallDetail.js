/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('CallDetail', {
    CallID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    InquiryID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ProspectID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CallDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    StartTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AgentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    CallTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CallNote: {
      type: DataTypes.STRING(1024),
      allowNull: true
    }
  }, {
    tableName: 'CallDetail'
  });
};

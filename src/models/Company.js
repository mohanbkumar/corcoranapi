/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Company', {
        id: {
            field: 'CompanyId',
            type: DataTypes.INTEGER(11),
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        CompanyName: {
            type: DataTypes.STRING(48),
            allowNull: true
        },
        CompanyType: {
            type: DataTypes.STRING(48),
            allowNull: true
        }
    }, {
            tableName: 'Company'
        });
};

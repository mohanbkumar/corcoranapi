/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('UserSite', {
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    }
  }, {
    tableName: 'UserSite'
  });
};

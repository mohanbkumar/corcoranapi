/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('FAQ', {
    FAQID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    Question: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    Answer: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    Catery: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false
    },
    SortOrder: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'FAQ'
  });
};

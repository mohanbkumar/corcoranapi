/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Site', {
    key: {
      field: 'ID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    SiteName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    SiteCode: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    Area: {
      type: DataTypes.STRING(36),
      allowNull: true
    },
    Address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    City: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    State: {
      type: DataTypes.CHAR(8),
      allowNull: true
    },
    PostalCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Fax: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    WebAddress: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SignedDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ExpirationDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesBeginDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ClosedDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    FirstDepositPercent: {
      type: "DOUBLE",
      allowNull: true
    },
    FirstDepositDays: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SecondDepositPercent: {
      type: "DOUBLE",
      allowNull: true
    },
    SecondDepositDays: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BrokerCommPercent: {
      type: "DOUBLE",
      allowNull: true
    },
    DevAttorneyFirstName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    DevAttorneyLastName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    DevAttorneyFirmName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAttorneyAddress: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAttorneyCity: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    DevAttorneyState: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    DevAttorneyZip: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    DevAttorneyPhone: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    DevAttorneyFax: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    DevAttorneyEmail: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MktShareRpt: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    EstTotalDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    EstTotalUnits: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EstAvgDollarsUnit: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    EstTotalSF: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    EstAvgDollarsSF: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    InquiryDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PitchDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CSMD: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ProfileStatus: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    MktName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    OwnerName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Developer1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Developer2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact1Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Contact1Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact2Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Contact2Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevType: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    DevComp: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    EstHeight: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TargetDOM: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ActDOM: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TargetTCO: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ActTCO: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Notes: {
      type: DataTypes.STRING(500),
      allowNull: true
    }
  }, {
      tableName: 'Site'
    });
};

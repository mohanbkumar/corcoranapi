/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
//  return sequelize.query('CALL sp_getAllAgents();').then(function(response){
//     res.json(response);
//    }).error(function(err){
//       res.json(err);
// });
  return sequelize.define('ManhattanLTM', {
    ManhattanLTMID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    LessThanOne: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OneToThree: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ThreeToFive: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FiveToTen: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TenToTwenty: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TwentyToForty: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    MoreThanForty: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    TotalUnsold: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'ManhattanLTM'
  });
  
};

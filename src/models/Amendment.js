/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Amendment', {
    AmendmentID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AmendmentDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AmendmentNum: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    AmendmentAmount: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'Amendment'
  });
};

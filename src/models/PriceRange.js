/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PriceRange', {
    key: {
      field: 'PriceRangeID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },    
    PriceRange: {
      type: DataTypes.STRING(16),
      allowNull: true
    }
  }, {
    tableName: 'PriceRange'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Peripheral', {
    key: {
      field: 'PeripheralID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }, 
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PeripheralTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PeripheralName: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    SQFT: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Price: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DateReleased: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Status: {
      type: DataTypes.STRING(5),
      allowNull: false
    }
  }, {
    tableName: 'Peripheral'
  });
};

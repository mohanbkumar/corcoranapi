/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Event', { 
    key: {
      field: 'EventID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },   
    Title: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Description: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    SiteIDList: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EventDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    StartTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    EndTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    MakeGlobal: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Event'
  });
};

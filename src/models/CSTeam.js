/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('CSTeam', {
    CSTeamID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ManagingDir: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    NewBus: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ResearchAssoc: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Planning: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ProjectCoord: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ProjectMan: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesDir1: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesDir1Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesDir1End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc1: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesAssoc1Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc1End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc2: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesAssoc2Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc2End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc3: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesAssoc3Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesAssoc3End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OfficeMan: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OfficeManStart: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OfficeManEnd: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord1: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OnSiteCoordStart: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoordEnd: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SiteProfileID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Acct1: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Acct2: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesDir1Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesAssoc1Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesAssoc2Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesAssoc3Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesDir2: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesDir2Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesDir2End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesDir2Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesDir3: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SalesDir3Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesDir3End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesDir3Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    WeekendAdmin: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ProjectDir: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OnSiteCoord2: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OnSiteCoord3: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OnSiteCoord1Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord1End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord1Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OnSiteCoord2Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord2End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord2Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    OnSiteCoord3Start: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord3End: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OnSiteCoord3Comm: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'CSTeam'
  });
};

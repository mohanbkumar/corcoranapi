/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('CallType', {
    CallTypeID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    CallType: {
      type: DataTypes.STRING(24),
      allowNull: true
    }
  }, {
    tableName: 'CallType'
  });
};

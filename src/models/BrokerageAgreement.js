/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('BrokerageAgreement', {
  
    key: {
      field: 'BrokerageAgreementID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }, 
    BrokerageID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AgreementSentDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AgreementSignedDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AgreementExpirationDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'BrokerageAgreement'
  });
};

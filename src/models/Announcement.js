/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Announcement', {
  
    key: {
      field: 'AnnouncementID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
    Title: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    Body: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    RoleIDList: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    SiteIDList: {
      type: DataTypes.STRING(400),
      allowNull: true
    },
    StartDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SelfDestructDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'Announcement'
  });
};

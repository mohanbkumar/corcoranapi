/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('VisitUnit', {
    VisitUnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    VisitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'VisitUnit'
  });
};

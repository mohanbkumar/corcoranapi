/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('Broker', {
    key: {
      field: 'BrokerID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    BrokerageID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    BrokerFirstName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    BrokerLastName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    Title: {
      type: DataTypes.STRING(40),
      allowNull: true
    },
    OfficePhone: {
      type: DataTypes.STRING(18),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.STRING(18),
      allowNull: true
    },
    BrokerEmail: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DoNotEmail: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    DoNotCall: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    DoNotDirectMail: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    VIP: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    BranchManager: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    BrokerOfRecord: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    NotLicensed: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    BrokerNotes: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    LicenseName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    PreferredName: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
      tableName: 'Broker'
    });
};

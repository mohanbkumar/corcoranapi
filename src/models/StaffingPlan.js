/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('StaffingPlan', {
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    CalendarDate: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    tableName: 'StaffingPlan'
  });
};

module.exports = function (sequelize, DataTypes) {
    return sequelize.define('Status', {
        id: {
            field: 'Id',
            type: DataTypes.INTEGER(11),
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
        Status: {
            type: DataTypes.STRING(24),
            allowNull: true
        },
    }, {
            tableName: 'Status'
        });
};

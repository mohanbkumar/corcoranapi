/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SaleStatus', {
    SaleStatusID: {
      type: DataTypes.STRING(2),
      allowNull: false
    },
    SaleStatus: {
      type: DataTypes.STRING(20),
      allowNull: true
    }
  }, {
    tableName: 'SaleStatus'
  });
};

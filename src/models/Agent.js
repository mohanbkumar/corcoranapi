/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Agent', {
    key: {
      field: 'AgentID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },   
    AgentFirstName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    AgentLastName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    AgentInitials: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Status: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Agent'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Inquiry', {
   
    key: {
      field: 'InquiryID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }, 
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    FirstName: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    LastName: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    Address: {
      type: DataTypes.STRING(96),
      allowNull: true
    },
    City: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    State: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    PostalCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    CountryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PhoneNumber: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    AgentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DoNotEmail: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    DoNotCall: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    BrokerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Broker2ID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ReferralSourceID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Note: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    InquiryDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    BrokerInquiry: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Inquiry'
  });
};

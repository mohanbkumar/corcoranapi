/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('SiteDev', {
    Id: {
      field: 'SiteDevID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DesignArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ExecArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    IntDesigner: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    LScapeArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CreativeAgency1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CreativeAgency2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    RenderFirm: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    PRFirm1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    PRFirm2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    MediaBuyer: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    SEOFirm: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    SponsorAttorney: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ManagingAgent: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
      tableName: 'SiteDev'
    });
};

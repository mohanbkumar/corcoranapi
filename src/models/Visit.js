/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Visit', {

    key: {
      field: 'VisitID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }, 
    ProspectID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    VisitDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    StartTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Revisit: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    UnitID: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    AgentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    BrokerID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Broker2ID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    VisitNote: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Visit'
  });
};

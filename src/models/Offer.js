/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Offer', {
    key: {
      field: 'OfferID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    }, 
    UnitID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    ProspectID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    OfferDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AskingPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    GrossOfferPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ClosingCostsRequested: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ConcessionsRequested: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    NetOfferPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    NetOfferPPSF: {
      type: "DOUBLE",
      allowNull: true
    },
    PercentDiscount: {
      type: "DOUBLE",
      allowNull: true
    },
    ClosingTimeframe: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    UnitDescription: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    UnitLineHistory: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    BuildingComparables: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    NeighborhoodComparables: {
      type: DataTypes.STRING(256),
      allowNull: true
    },
    Recommendation: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    SponsorComments: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    AdditionalNotes: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    OfferStatus: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Offer'
  });
};

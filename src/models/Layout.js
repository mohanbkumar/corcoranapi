/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Layout', {
    key: {
      field: 'LayoutID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },    
    LayoutName: {
      type: DataTypes.STRING(24),
      allowNull: true
    }
  }, {
    tableName: 'Layout'
  });
};

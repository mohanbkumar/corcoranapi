/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Directory', {
     key: {
      field: 'PersonID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },   
    FirstName: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    LastName: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    Title: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    Location: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    Address1: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Address2: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    City: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    State: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    Zip: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    OfficePhone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Mobile: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Fax: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Biography: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    HouseID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Directory'
  });
};

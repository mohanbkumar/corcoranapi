/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('StageUnitTest', {
    Num: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UnitName: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    Building: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    FloorNumber: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    Line: {
      type: DataTypes.STRING(4),
      allowNull: true
    },
    LayoutID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    RentalStatus: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    ContractStatus: {
      type: DataTypes.STRING(16),
      allowNull: true
    },
    DateReleased: {
      type: DataTypes.DATE,
      allowNull: true
    },
    OrigAskingPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AskingPriceDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    AskingPrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PurchasePrice: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SqFt: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    OutdoorSQFT: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Superintendent: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    Rooms: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    BR: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    BAF: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    BAH: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    FinishType: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    Exposure: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    UnitNote: {
      type: DataTypes.STRING(800),
      allowNull: true
    },
    Status: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    Bldg: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    Flr: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Sect: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    StartCell: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    CellSpan: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    FlrSpan: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    }
  }, {
    tableName: 'StageUnitTest'
  });
};

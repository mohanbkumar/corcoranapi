/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Residence', {
    key: {
      field: 'ResidenceID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },    
    Residence: {
      type: DataTypes.STRING(36),
      allowNull: true
    }
  }, {
    tableName: 'Residence'
  });
};

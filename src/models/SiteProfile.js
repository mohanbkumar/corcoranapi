/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SiteProfile', {
    SiteProfileID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    InquiryDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    PitchDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CSMD: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Developer1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Developer2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    OwnerName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    MktName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact1Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Contact1Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Contact2Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Contact2Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ProfileStatus: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    DevType: {
      type: DataTypes.STRING(80),
      allowNull: true
    },
    DevComp: {
      type: DataTypes.STRING(300),
      allowNull: true
    },
    EstNetSalable: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EstNumUnits: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EstDollarsSQFT: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    EstSellout: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    EstHeight: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    DesignArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ExecArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    IntDesigner: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    LScapeArch: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CreativeAgency1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    RenderFirm: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    PRFirm1: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ProfileNotes: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    TargetDOM: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ActDOM: {
      type: DataTypes.DATE,
      allowNull: true
    },
    TargetTCO: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ActTCO: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    City: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    State: {
      type: DataTypes.CHAR(8),
      allowNull: true
    },
    Zip: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Phone: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    WebAddress: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Area: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CreativeAgency2: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    PRFirm2: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    tableName: 'SiteProfile'
  });
};

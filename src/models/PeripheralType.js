/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PeripheralType', {
  key: {
      field: 'PeripheralTypeID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },

    PeripheralType: {
      type: DataTypes.STRING(24),
      allowNull: true
    }
  }, {
    tableName: 'PeripheralType'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Users', {
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    RoleID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Username: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    Password: {
      type: DataTypes.STRING(12),
      allowNull: false
    },
    FirstName: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    LastName: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    Title: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Phone: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Status: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'Users'
  });
};

/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PeripheralSale', {
    PeripheralSaleID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    PeripheralID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    SaleID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AskingPrice: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    PurchasePrice: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SaleDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    Status: {
      type: DataTypes.CHAR(1),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    tableName: 'PeripheralSale'
  });
};

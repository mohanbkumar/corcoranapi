/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('PurchaseReason', {
    key: {
      field:'PurchaseReasonID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },    
    PurchaseReason: {
      type: DataTypes.STRING(24),
      allowNull: true
    }
  }, {
    tableName: 'PurchaseReason'
  });
};

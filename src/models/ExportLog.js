/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ExportLog', {
    ExportLogID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    UserID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    RequestDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ReportType: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    QType: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    OutputFormat: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    SearchParameters: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    NumRecords: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'ExportLog'
  });
};

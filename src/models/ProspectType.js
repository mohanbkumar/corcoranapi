/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ProspectType', {
    key: {
      field: 'ProspectTypeID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    ProspectTypeName: {
      type: DataTypes.STRING(50),
      allowNull: true
    }
  }, {
    tableName: 'ProspectType'
  });
};

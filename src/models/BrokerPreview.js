/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('BrokerPreview', {
    BrokerPreviewID: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    BrokerID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    VisitDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    StartTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    UnitID: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    AgentID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Notes: {
      type: DataTypes.STRING(1024),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'BrokerPreview'
  });
};

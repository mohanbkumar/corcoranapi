/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('SiteLegal', {
    key: {
      field: 'ID',
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      autoIncrement: true
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    SiteProfileID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    DevName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAddress: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    Ownership: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Marketing: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAttorneyFirstName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    DevAttorneyLastName: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    DevAttorneyFirmName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAttorneyAddress: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DevAttorneyCity: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    DevAttorneyState: {
      type: DataTypes.CHAR(2),
      allowNull: true
    },
    DevAttorneyZip: {
      type: DataTypes.STRING(12),
      allowNull: true
    },
    DevAttorneyPhone: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    DevAttorneyFax: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    DevAttorneyEmail: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    ProposalOutDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ProposalSignedDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ESMAOutDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ESMASignedDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SalesCommDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ESMAExpDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    ESMAExtDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    IncTriggerDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    InsTriggerDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    BranchLicense: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    BranchExpDate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    SubAgree: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    LenderName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    CommRateBroke: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    CommRateDirect: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    SalesPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    AdminPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ConsultAmt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ConsultFreq: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    ConsultCap: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    ConsultCapDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ConsultCredited: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    ConsultPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PlanAmt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PlanFreq: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    PlanCap: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    PlanCapDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    PlanCredited: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    PlanPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    MktAmt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    MktFreq: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    MktCap: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    MktCapDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    MktCredited: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    MktPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LossAmt: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LossFreq: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    LossCap: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    LossCapDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    LossCredited: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    LossPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ProposalExpired: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    Incentive: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    Insurance: {
      type: DataTypes.CHAR(1),
      allowNull: true
    },
    ExcludedUnits: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    ExcludedUnitsDollars: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    TerminationFee: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    DepositPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    },
    ExcludedUnitsPercent: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
      tableName: 'SiteLegal'
    });
};

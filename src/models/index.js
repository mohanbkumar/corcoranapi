'use strict';

var fs = require('fs'),
  path = require('path'),
  util = require('util'),
  Sequelize = require('sequelize'),
  basename = path.basename(module.filename),
  env = process.env.NODE_ENV || 'development';

var db = {};

const Op = Sequelize.Op;

const DATABASE_URL = util.format(
  "mysql://%s:%s@%s:%s/%s",
  process.env.DATABASE_USER,
  process.env.DATABASE_PASSWORD,
  process.env.DATABASE_SERVER,
  process.env.DATABASE_PORT,
  process.env.DATABASE_NAME
);


var sequelize = new Sequelize(DATABASE_URL, {
  // disable logging; default: console.log
  // logging: function (data) {
  //   console.log(data);
  // },
  logging: false,
  operatorsAliases: Op,
  dialectOptions: {
    encrypt: process.env.SECURE_DATABASE_CONNECTION || false
  },
  define: {
    //prevent sequelize from pluralizing table names
    timestamps: false,
    freezeTableName: true
  },

  pool: {
    maxConnections: 5,
    maxIdleTime: 30
  }
});
sequelize.getCurrentDatetime = function () {
  var currentDatetimeCST = new Date();
  var currentDatetimeGST = new Date(currentDatetimeCST - (currentDatetimeCST.getTimezoneOffset() * 60000));
  return currentDatetimeGST.toISOString();
};

//Load all the models
fs
  .readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(function (file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db
  .Users
  .hasMany(db.Role, {
    as: 'UsersRole',
    foreignKey: {
      name: 'RoleID'
    },
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

db
  .Site
  .hasMany(db.SiteDev, {
    as: 'SiteSiteDev',
    foreignKey: {
      name: 'SiteID'
    },
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

db
  .Site
  .hasMany(db.SiteLegal, {
    as: 'SiteLegal',
    foreignKey: {
      name: 'SiteID'
    },
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION'
  });

sequelize
  .sync()
  .then(function (err) {
    console.log('Model Synced to database!');
  }, function (err) {
    console.log('An error occurred while creating the table:', err);
  });

 
//Export the db Object
db.sequelize = sequelize;

module.exports = db;
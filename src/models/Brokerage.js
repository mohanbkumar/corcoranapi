/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Brokerage', {
    key: {
      field: 'BrokerageID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  
  
    BrokerageName: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    Branch: {
      type: DataTypes.STRING(60),
      allowNull: true
    },
    Address: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    City: {
      type: DataTypes.STRING(24),
      allowNull: true
    },
    State: {
      type: DataTypes.STRING(8),
      allowNull: true
    },
    PostalCode: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    CountryID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    Phone: {
      type: DataTypes.STRING(18),
      allowNull: true
    },
    Fax: {
      type: DataTypes.STRING(18),
      allowNull: true
    },
    Email: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'Brokerage'
  });
};

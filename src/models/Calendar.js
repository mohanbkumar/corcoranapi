/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('Calendar', {
    key: {
      field: 'dt',
      type: DataTypes.DATE,
      allowNull: false,
      primaryKey: true
      },     
    IsWeekday: {
      type: DataTypes.INTEGER(4),
      allowNull: true
    },
    IsHoliday: {
      type: DataTypes.INTEGER(4),
      allowNull: false
    },
    Yr: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    FiscalYr: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    },
    Qtr: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    Mnth: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    Dy: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    DyOfWeek: {
      type: DataTypes.INTEGER(3).UNSIGNED,
      allowNull: true
    },
    MnthName: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    MnthNameShort: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    DyName: {
      type: DataTypes.STRING(9),
      allowNull: true
    },
    DyNameShort: {
      type: DataTypes.STRING(3),
      allowNull: true
    },
    WkNumber: {
      type: DataTypes.INTEGER(6),
      allowNull: true
    }
  }, {
    tableName: 'Calendar'
  });
};

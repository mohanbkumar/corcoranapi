/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ReferralSource', {
 
    key: {
      field: 'ReferralSourceID',
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    SourceName: {
      type: DataTypes.STRING(48),
      allowNull: true
    },
    SourceType: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    SiteID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    Status: {
      type: DataTypes.INTEGER(6),
      allowNull: false
    },
    DateCreated: {
      type: DataTypes.DATE,
      allowNull: true
    },
    CreatedByID: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }
  }, {
    tableName: 'ReferralSource'
  });
};
